extends Node2D

func _physics_process(_delta):
	handle_movement()


func handle_movement():
	if Input.is_action_pressed("ui_right"):
		position.x += 5
	if Input.is_action_pressed("ui_left"):
		position.x -= 5
	if Input.is_action_pressed("ui_up"):
		position.y -= 5
	if Input.is_action_pressed("ui_down"):
		position.y += 5
		
	clamp_on_screen()


func clamp_on_screen():
	position.x = clamp(position.x, 0, get_viewport_rect().size.x)
	position.y = clamp(position.y, 0, get_viewport_rect().size.y)


func _on_player_area_entered(_area):
	print("lost")
