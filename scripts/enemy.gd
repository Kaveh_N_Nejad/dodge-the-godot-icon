extends Node2D

var movement = Vector2()


func _ready():
	randomize()
	set_origional_position()
	set_origiona_movement()


func _physics_process(_delta):
	move()
	handle_hitting_wall()


func bounce_x():
	movement.x = -movement.x


func bounce_y():
	movement.y = -movement.y


func set_origional_position():
	position = Vector2(100, 100)


func set_origiona_movement():
	movement = Vector2(random_number(), random_number())


func random_number():
	return randf() * 10 - 5


func move():
	position += movement


func handle_hitting_wall():
	if position.x > get_viewport_rect().size.x or position.x < 0:
		bounce_x()
	elif position.y > get_viewport_rect().size.y or position.y < 0:
		bounce_y()
