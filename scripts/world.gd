extends Node2D

export(PackedScene) var enemy_scene

func spawn_enemy():
	var new_enemy = enemy_scene.instance()
	add_child(new_enemy)


func _on_Timer_timeout():
	spawn_enemy()
